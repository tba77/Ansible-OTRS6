Role Name
=========

Install community version of OTRS6 on last centos7

Requirements
------------

No Requirements

Role Variables
--------------

mysql_password : mysql root password, you must specify a password that will be set after installation complete

Dependencies
------------

No Dependencies

Example Playbook
----------------

ansible-playbook -i /path/to/your/inventory /path/to/site.yml --extra-vars "remote_host=inventory_host"

playbook: 

```
---
- name: install otrs on centos7
  hosts: "{{ remote_host }}"
  remote_user: vagrant
  become: true
  
  roles:
    - common
    - otrs
```


License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
